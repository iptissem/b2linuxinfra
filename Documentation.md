# Projet Infra

## Sommaire 

- [Projet Infra](#projet-Infra)
  - [Sommaire](#sommaire)
  - [Présentation du besoin](#Présentation-du-besoin)
  - [Présentation des solutions](#Présentation-des-soltuions)
    - [Mise en place des soltuions](#Mise-en-place-des-soltuions)
      - [Serveur FTP](#Serveur-Ftp)
      - [Serveur Syslog](#etablir-un-serveur-Syslog)
      - [Serveur DNS](#Serveur-Dns)
      - [Serveur DHCP](#Serveur-Dhcp)
      - [Monitoring et backup](#Monitoring)
      - [Firewall ufw](#Firewall-ufw)
      
## Présentation du besoin

Nous allons à travers ce projet, créer un réseau de serveurs fonctionelles pour une startup de jeux vidéos.
Cette entreprise à besoin de serveurs essentiels aux fonctionnements d'une entreprise.

## Présentation des soltuions

Pour se faire, nous installerons plusieurs serveurs avec des fonctionnalités bien distinctes : 
- Serveur FTP pour les transfères de fichiers.
- Serveur Syslog pour la centralisation des logs de nos machines.
- Serveur DNS qui traduit les adresses ip en nom de domaine.
- Serveur DHCP qui distribue les adresses ip aux machines du réseau.


- Schéma de nos machines

 5 machines sous Debian, avec 1 giga de RAM et 20GB CPU.

![](schema.png)

## Mise en place des solutions

### Installation et configuration serveur FTP

#### - Installation vsftpd

### Installation et configuration serveur Syslog

#### - Installation syslog-ng

### Installation et configuration serveur DNS

#### - Installation DNS primaire

plusieurs paquets sont essenitels : 
• bind9 (Paquets bind9)
• bind9-doc (Documentation de bind)
• bind9utils (utilitaire additionnels pour bind9 (rndc))
• resolvconf (Paquets de gestion dynamique du resolv.conf)
• dnsutils (Paquets contentant les commandes nslookup et dig entre autre)
